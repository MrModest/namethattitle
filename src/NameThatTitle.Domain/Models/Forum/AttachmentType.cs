﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NameThatTitle.Domain.Models.Forum
{
    public enum AttachmentType
    {
        Image,
        Video
    }
}
