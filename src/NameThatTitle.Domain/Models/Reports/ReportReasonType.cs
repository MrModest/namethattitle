﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NameThatTitle.Domain.Models.Reports
{
    public class ReportReasonType : BaseEntity
    {
        public string Name { get; set; }
    }
}
